/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 * 50 рутин параллельно вызывают db.get(1) - к базе должен быть только один запрос
 * все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 * 50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть только один
 */

package simplemod

import (
	"bitbucket.org/sergey-zhuravlev/in-memory-db/counter"
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var errNotFound = errors.New("db: not found")
var errNetworkFailed = errors.New("db: network failed")
var errNetworkMaxRetries = errors.New("db: maximum retries exceeded")

type User struct {
	id uint64
	name string
	uniq uint64
	err error
}

type stored struct {
	mu sync.RWMutex
	flag map[uint64]bool
}

type DB struct {
	mu sync.RWMutex
	users map[uint64]*User
	maxRetries int  // Максимальное повторное кол-во попыток при ошибке errNetworkFailed
	stored // хэш с локом для отслеживания уже сохранённых пользователей
	UsersNumber uint64
	Hc counter.Counter
}

/*
 *
 * TODO: реализовать thead-safe метод получения объекта из базы данных и не забыть про корректную обработку ошибок:
 * - notFound - сохранять как ответ в кеше
 * - networkError - не сохранять как ответ и идти в сеть снова
 */
func (db *DB) fetchUser(id uint64) {
	//fmt.Printf("        user#%d fetching\n", id)
	var user *User
	var err error
	// Выполняем повторное обращение к БД в случае ошибки errNetworkFailed, кол-во повторов ограничено db.maxRetries
	for retries := 0; retries < db.maxRetries; retries ++ {
		db.Hc.Count(id)  // считаем обращения к БД для отдельного пользователя
		user, err = db.fetch(id)
		if err != errNetworkFailed {
			break
		}
		db.Hc.Decount(id)  // повторное обращение из-за ошибки сети не считается за хит БД
		if retries == db.maxRetries -1 {
			err = errNetworkMaxRetries
		}
	}

	if err != nil {
		user = &User{
			id: id,
			name: err.Error(),
			err: err,  // Сохраняем ошибку извлечения данных пользователя в кэше
		}
	}

	//Блокируем структуру только на запись и обновляем данные пользователя
	db.mu.Lock()
	db.users[id] = user
	db.mu.Unlock()
	//fmt.Printf("        user#%d done fetching\n", id)
}

func (db *DB) get(id uint64) (*User, error) {
	db.mu.RLock()
	user, ok := db.users[id]
	db.mu.RUnlock()
	if !ok {
		db.mu.Lock()
		newUser := &User{}
		db.users[id] = newUser
		db.mu.Unlock()
		user = newUser
		go db.fetchUser(id)
	}
	for user.name == "" {
		time.Sleep(10*time.Millisecond)
		db.mu.RLock()
		user = db.users[id]
		db.mu.RUnlock()
	}
	if user.err != nil {
		return user, user.err
	}
	return user, nil
}

/*
 * TODO: реализовать thread-safe метод
 */
func (db *DB) putUser(user *User) {
	_ = db.store(user)
}

func (db *DB) put(user *User) {
	db.stored.mu.RLock()
	_, ok := db.stored.flag[user.id]
	db.stored.mu.RUnlock()
	if !ok {
		db.stored.mu.Lock()
		db.stored.flag[user.id] = true
		db.stored.mu.Unlock()
		go db.putUser(user)
	}
}

/*
 * fetch - возвращает объект по ID из базы, симулирует
 */
func (db *DB) fetch(id uint64) (*User, error) {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNotFound
	}

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNetworkFailed
	}

	return &User{
		id: id,
		name: fmt.Sprintf("user#%d", id),
		uniq: rand.Uint64(),
	}, nil
}

/* симулирует сохранение объекта через сеть - т.е. делает это с задержкой и иногда возвращает ошибку */
func (db *DB) store(user *User) error {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return errNetworkFailed
	}

	// all ok
	return nil
}

func NewDB(usersNumber uint64) *DB {
	return &DB{
		users: make(map[uint64]*User),
		maxRetries: 5,
		stored: stored{flag: make(map[uint64]bool)},
		UsersNumber: usersNumber,
		Hc: counter.Counter{
			Hits: make(map[uint64]int),
		},
	}
}

func (db *DB) SimpleMod() {
	rand.Seed(time.Now().UTC().UnixNano())

	wg := sync.WaitGroup{}
	for i := 0; i < 50; i ++ {
		wg.Add(1)
		go func() {
			uid := uint64(rand.Int63n(int64(db.UsersNumber)))
			user, err := db.get(uid)
			if err != nil {
				fmt.Printf("user#%d: %#v\n", uid, err)
			} else {
				fmt.Printf("user#%d: uniq %d\n", user.id, user.uniq)
				time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
				db.put(user)
			}
			wg.Done()
		}()
		wg.Wait()
	}
}
