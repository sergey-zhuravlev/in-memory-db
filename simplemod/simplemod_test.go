package simplemod

import (
	"fmt"
	"testing"
)

func TestSimpleMod(t *testing.T)  {
	db := NewDB(50)
	//db := NewDB(2)
	db.SimpleMod()

	err := db.Hc.CheckHits()
	if err != nil {
		err = fmt.Errorf("get DB hit counter error - %v", err)
		t.Error(err)
	}
}
