/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 * 50 рутин параллельно вызывают db.get(1) - к базе должен быть только один запрос
 * все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 * 50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть только один
 */

package main

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var errNotFound = errors.New("db: not found")
var errNetworkFailed = errors.New("db: network failed")
var errCacheNotFound = errors.New("cache: not found")
var errNetworkMaxRetries = errors.New("db: maximum retries exceeded")

type User struct {
	id uint64
	name string
	uniq uint64
	err error  // Кэшируем возможную ошибку при обновлении данных пользователя
}

// Структура для асинхронной реализации очередей запросов к БД
type queue struct {
	mu sync.Mutex
	workHardAsyncHarder map[uint64]uint64
}

type DB struct {
	usersNumber uint64
	users map[uint64]*User
	mu sync.RWMutex  // Мьютекс блокирующий только операции записи
	getQ queue
	putQ queue
	maxRetries int  // Максимальное повторное кол-во попыток при ошибке errNetworkFailed
}

/*
 *
 * TODO: реализовать thead-safe метод получения объекта из базы данных и не забыть про корректную обработку ошибок:
 * - notFound - сохранять как ответ в кеше
 * - networkError - не сохранять как ответ и идти в сеть снова
 */

func (db *DB) getUser(id uint64) {
	db.getQ.mu.Lock()
	_, ok := db.getQ.workHardAsyncHarder[id]
	if ok {
		//fmt.Printf("    user#%d GET already running\n", id)
		// Если значение в хеше найдено, разблокируем и выходим так как `get` для данного пользователя уже в процессе
		db.getQ.mu.Unlock()
		return
	}
	db.getQ.workHardAsyncHarder[id] = 0
	db.getQ.mu.Unlock()

	//fmt.Printf("        user#%d DB hit happened\n", id)
	var user *User
	var err error
	// Выполняем повторное обращение к БД в случае ошибки errNetworkFailed, кол-во повторов ограничено db.maxRetries
	for retries := 0; retries < db.maxRetries; retries ++ {
		user, err = db.fetch(id)
		if err != errNetworkFailed {
			break
		}
		if retries == db.maxRetries -1 {
			err = errNetworkMaxRetries
		}
	}

	if err == nil {
		user.err = nil  // обнуляем ошибку у пользователя
	}
	if err != nil {
		user = &User{
			id: id,
			err: err,  // сохраняем ошибку извлечения данных пользователя в кэше
		}
	}
	// Блокируем структуру только на запись и обновляем данные пользователя
	db.mu.Lock()
	db.users[id] = user
	db.mu.Unlock()

	// Блокируем очередь и удаляем флаг `в работе`
	db.getQ.mu.Lock()
	delete(db.getQ.workHardAsyncHarder, id)
	db.getQ.mu.Unlock()
	//fmt.Printf("        user#%d cache updated\n", id)
}

func (db *DB) get(id uint64) (*User, error) {
	// Отправляем асинхронный запрос в воркер на обновления данных пользователя из БД.
	go db.getUser(id)

	// Практически мгновенно отдаём данные пользователя из кэша:
	// защищаемся от записи в момент чтения, не блокирует другие операции чтения так как используем RWMutex
	db.mu.Lock()
	user := db.users[id]
	db.mu.Unlock()
	// Обрабатываем вариант когда пользователь в кэше не найден.
	if user == nil {
		return nil, errCacheNotFound
	}
	// Обрабатываем закэшированную ошибку.
	if user.err != nil {
		return nil, user.err
	}
	return user, nil
}

/*
 * TODO: реализовать thread-safe метод
 */
func (db *DB) putUser(user *User) {
	db.putQ.mu.Lock()
	uniq, ok := db.putQ.workHardAsyncHarder[user.id]
	if ok {
		//fmt.Printf("    user#%d PUT already running with uniq %d - new uniq %d\n", user.id, uniq, user.uniq)

		// Есть мини вероятность, что почти одновременно прилетели обновления с разным содержимом `user`, проверяем:
		if uniq != user.uniq {
			fmt.Printf("WARNING: uniq has been updated, current %d - new %d\n", uniq, user.uniq)
		}

		// Если значение в хеше найдено, разблокируем и выходим так как `put` для данного пользователя уже в процессе
		db.putQ.mu.Unlock()
		return
	}
	db.putQ.workHardAsyncHarder[user.id] = user.uniq
	db.putQ.mu.Unlock()

	//fmt.Printf("        user#%d DB UPDATE started\n", user.id)
	_ = db.store(user)

	// Блокируем очередь и удаляем флаг `в работе`
	db.putQ.mu.Lock()
	delete(db.putQ.workHardAsyncHarder, user.id)
	db.putQ.mu.Unlock()
	//fmt.Printf("        user#%d DB updated\n", user.id)
}

func (db *DB) put(user *User) {
	go db.putUser(user)
}

/*
 * fetch - возвращает объект по ID из базы, симулирует
 */
func (db *DB) fetch(id uint64) (*User, error) {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNotFound
	}

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNetworkFailed
	}

	return &User{
		id: id,
		name: fmt.Sprintf("user#%d", id),
		uniq: rand.Uint64(),
	}, nil
}

/* Симулирует сохранение объекта через сеть - т.е. делает это с задержкой и иногда возвращает ошибку */
func (db *DB) store(user *User) error {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return errNetworkFailed
	}

	// all ok
	return nil
}

/*
 * initCache - симулирует быструю инициализацию кэша начальными значениями
 */
func (db *DB) initCache() {
	for i := 0; i < int(db.usersNumber); i++ {
		id := uint64(i)
		db.users[id] = &User{
			id: id,
			name: fmt.Sprintf("user#%d", id),
			uniq: rand.Uint64(),
		}
	}
}

func newDB(usersNumber uint64) *DB {
	db := &DB{
		usersNumber: usersNumber,
		users: make(map[uint64]*User),
		maxRetries: 5,
	}
	db.putQ.workHardAsyncHarder = make(map[uint64]uint64)
	db.getQ.workHardAsyncHarder = make(map[uint64]uint64)

	// Так как задача сразу отдавать пользователя из кэша, можно как-то проинициализировать кэш при создании БД.
	// Можно этого не делать, тогда при первых запросах вернётся ошибка кэша errCacheNotFound.
	db.initCache()

	return db
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	db := newDB(50)

	// example usage
	for j := 0; j < 4; j ++ {  // Увеличиваем кол-во итераций запросов к БД
		fmt.Printf("##### iteration %02d ###################################\n", j)
		for i := 0; i < 50; i++ {
			go func() {
				uid := uint64(rand.Int63n(int64(db.usersNumber)))
				user, err := db.get(uid)
				if err != nil {
					fmt.Printf("user#%d: %#v\n", uid, err)
				} else {
					fmt.Printf("user#%d: uniq %d\n", user.id, user.uniq)
					time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
					db.put(user)
				}
			}()
		}
		time.Sleep(100*time.Millisecond)
		fmt.Printf("##### iteration %02d ended #############################\n", j)
		time.Sleep(2000 * time.Millisecond)
	}
}
