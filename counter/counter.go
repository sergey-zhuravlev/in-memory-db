package counter

import (
	"fmt"
	"sync"
)

type Counter struct {
	mu sync.RWMutex
	Hits map[uint64]int
}

func (c *Counter) Count(id uint64) int {
	c.mu.Lock()
	c.Hits[id] += 1
	hits := c.Hits[id]
	c.mu.Unlock()
	return hits
}

func (c *Counter) Decount(id uint64)  {
	c.mu.Lock()
	c.Hits[id] -= 1
	c.mu.Unlock()
}

func (c *Counter) Get(id uint64) int {
	c.mu.RLock()
	hits := c.Hits[id]
	c.mu.RUnlock()
	return hits
}

func (c *Counter) GetHits() map[uint64]int {
	c.mu.RLock()
	hits := c.Hits
	c.mu.RUnlock()
	return hits
}

func (c *Counter) CheckHits() error {
	c.mu.RLock()
	for k, hit := range c.Hits {
		if hit > 1 {
			msg := fmt.Sprintf("user #%d - got hits %d, need hits 1", k, hit)
			return fmt.Errorf(msg)
		}
	}
	c.mu.RUnlock()
	return nil
}
