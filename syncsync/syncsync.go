/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 * 50 рутин параллельно вызывают db.get(1) - к базе должен быть только один запрос
 * все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 * 50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть только один
 */

package syncsync

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

var errNotFound = errors.New("db: not found")
var errNetworkFailed = errors.New("db: network failed")

type User struct {
	id uint64
	name string
	uniq uint64
}

type MetaUser struct {
	proto *User
	err error
}

type once struct {
	mu sync.Mutex
	done uint32
	err error
}

type locks struct {
	mu sync.Mutex
	once map[uint64]*once
}

type DB struct {
	mu sync.RWMutex
	users map[uint64]*MetaUser
	usersNumber int
	gl locks
	pl locks
}

/*
 *
 * TODO: реализовать thead-safe метод получения объекта из базы данных и не забыть про корректную обработку ошибок:
 * - notFound - сохранять как ответ в кеше
 * - networkError - не сохранять как ответ и идти в сеть снова
 */
func (db *DB) fetchCacheUser(id uint64) {
	fmt.Printf("        user#%d fetching\n", id)
	proto, err := db.fetch(id)
	if err != nil {
		if err == errNotFound {
			// Ошибка поиска пользователя - создаём нового, ниже сохраняем ошибку в кэш.
			proto = &User{ id: id }
		} else {
			// Ошибка сети - записываем ошибку и выходим из функции.
			db.gl.once[id].err = err
			return
		}
	}
	// Записываем результат в кэш, включая значение ошибки.
	db.mu.Lock()
	db.users[id] = &MetaUser{
		proto: proto,
		err: err,
	}
	db.mu.Unlock()
}

func (db *DB) getUser(id uint64) {
	if atomic.LoadUint32(&db.gl.once[id].done) == 0 {
		db.gl.once[id].mu.Lock()
		defer db.gl.once[id].mu.Unlock()
		// Повторно проверяем значение для конкурентно выполняющихся рутин, которые захватят лок следующими.
		if db.gl.once[id].done == 0 {
			defer atomic.StoreUint32(&db.gl.once[id].done, 1)
			db.fetchCacheUser(id)
		}
	}
}

func (db *DB) get(id uint64) (*User, error) {
	// Добавляем once-локер для нового пользователя, если его ещё нет
	db.gl.mu.Lock()
	_, ok := db.gl.once[id]
	if !ok {
		db.gl.once[id] = &once{}
	}
	db.gl.mu.Unlock()
	db.getUser(id)

	// Пытаемся получить значение пользователя из кэша.
	db.mu.RLock()
	metaUser, ok := db.users[id]
	db.mu.RUnlock()
	// Если не находим пользователя, выходим и возвращаем записанную ошибку (но не из кэша пользователя).
	if !ok {
		return nil, db.gl.once[id].err
	}
	// Проверяем на ошибку в кэше пользователя. Возвращаем при наличии.
	if metaUser.err != nil {
		return nil, metaUser.err
	}
	return metaUser.proto, nil
}

/*
 * TODO: реализовать thread-safe метод
 */
func (db *DB) putUser(user *User) {
	if atomic.LoadUint32(&db.pl.once[user.id].done) == 0 {
		db.pl.once[user.id].mu.Lock()
		defer db.pl.once[user.id].mu.Unlock()
		// Повторно проверяем значение для конкурентно выполняющихся рутин, которые захватят лок следующими.
		if db.pl.once[user.id].done == 0 {
			defer atomic.StoreUint32(&db.pl.once[user.id].done, 1)
			fmt.Printf("        user#%d putting\n", user.id)
			_ = db.store(user)
		}
	}
}

func (db *DB) put(user *User) {
	// Добавляем once-локер для нового пользователя, если его ещё нет
	db.pl.mu.Lock()
	_, ok := db.pl.once[user.id]
	if !ok {

		db.pl.once[user.id] = &once{}
	}
	db.pl.mu.Unlock()
	db.getUser(user.id)

	db.putUser(user)
}

/*
 * fetch - возвращает объект по ID из базы, симулирует
 */
func (db *DB) fetch(id uint64) (*User, error) {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNotFound
	}

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNetworkFailed
	}

	return &User{
		id: id,
		name: fmt.Sprintf("user#%d", id),
		uniq: rand.Uint64(),
	}, nil
}

/* симулирует сохранение объекта через сеть - т.е. делает это с задержкой и иногда возвращает ошибку */
func (db *DB) store(user *User) error {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return errNetworkFailed
	}

	// all ok
	return nil
}

func NewDB(usersNumber int) *DB {
	db := &DB{
		users: make(map[uint64]*MetaUser),
		usersNumber: usersNumber,
		gl: locks{once: make(map[uint64]*once)},
		pl: locks{once: make(map[uint64]*once)},
	}
	return db
}

func (db *DB) SyncSync() {
	rand.Seed(time.Now().UTC().UnixNano())

	wg := sync.WaitGroup{}
	for i := 0; i < 500; i ++ {
		wg.Add(1)
		go func() {
			uid := uint64(rand.Int63n(int64(db.usersNumber)))
			user, err := db.get(uid)
			if err != nil {
				fmt.Printf("user#%d: %#v\n", uid, err)
			} else {
				fmt.Printf("user#%d: uniq %d\n", user.id, user.uniq)
				time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
				db.put(user)
			}
			wg.Done()
		}()
	}
	wg.Wait()
	// Очищаем память, что можем.
	for k := range db.gl.once {
		delete(db.gl.once, k)
	}
}
