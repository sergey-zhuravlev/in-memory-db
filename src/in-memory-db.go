/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 *  50 рутин паралеллеьно вызывают db.get(1) - к базе дожлен быть только один запрос
 *  все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 *  50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть отлько один
 */

package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

var errNotFound = errors.New("db: not found")
var errNetworkFailed = errors.New("db: network failed")

type User struct {
	id uint64
	name string
	uniq uint64
}

type DB struct {
	users map[uint64]*User
}

/*
 * 
 * TODO: реализовать thead-safe метод получения объекта из базы данных и не забыть про корректную обработку ошибок:
 * - notFound - сохранять как ответ в кеше
 * - networkError - не сохранять как ответ и идти в сеть снова
 */
func (db *DB) get(id uint64) (*User, error) {
	user, err := db.fetch(id)
	if err != nil {
		return nil, err
	}

	db.users[id] = user
	return user, nil
}

/*
 * TODO: реализовать thread-safe метод
 */
func (db *DB) put(user *User) {
	_ = db.store(user)
}

/*
 * fetch - возвращает объект по ID из базы, симулирует
 */
func (db *DB) fetch(id uint64) (*User, error) {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNotFound
	}

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNetworkFailed
	}

	return &User{
		id: id,
		name: fmt.Sprintf("user#%d", id),
		uniq: rand.Uint64(),
	}, nil
}

/* симулирует сохранение объекта через сеть - т.е. делает это с задержкой и иногда возвращает ошибку */
func (db *DB) store(user *User) error {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return errNetworkFailed
	}

	// all ok
	return nil
}

func newDB() *DB {
	return &DB{
		users: make(map[uint64]*User),
	}
}

func main() {
	db := newDB()
	
	// example usage
	for i := 0; i < 50; i ++ {
		go func() {
			uid := uint64(rand.Int63n(50))
			user, err := db.get(uid)
			if err != nil {
				fmt.Printf("user#%d: %#v", uid, err)
			} else {
				time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
				db.put(user)
			}
		}()
	}
	
	time.Sleep(10)
}
