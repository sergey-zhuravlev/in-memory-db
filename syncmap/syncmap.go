/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 * 50 рутин параллельно вызывают db.get(1) - к базе должен быть только один запрос
 * все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 * 50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть только один
 */

package syncmap

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

var errNotFound = errors.New("db: not found")
var errNetworkFailed = errors.New("db: network failed")

type User struct {
	id uint64
	name string
	uniq uint64
}

type MetaUser struct {
	proto *User
	err error
}

type once struct {
	done uint32
	m sync.Mutex
}

type DB struct {
	mu sync.RWMutex
	users map[uint64]*MetaUser
	usersNumber int
	getLock sync.Map
	putLock sync.Map
}
/*
 *
 * TODO: реализовать thead-safe метод получения объекта из базы данных и не забыть про корректную обработку ошибок:
 * - notFound - сохранять как ответ в кеше
 * - networkError - не сохранять как ответ и идти в сеть снова
 */
func (db *DB) fetchCacheUser(id uint64) {
	//fmt.Printf("        user#%d fetching\n", id)
	proto, err := db.fetch(id)
	if err != nil {
		if err == errNotFound {
			// Ошибка поиска пользователя - создаём нового, ниже сохраняем ошибку в кэш.
			proto = &User{ id: id }
		} else {
			// Ошибка сети - выходим из функции.
			return
		}
	}
	// Записываем результат в кэш, включая значение ошибки.
	db.mu.Lock()
	db.users[id] = &MetaUser{
		proto: proto,
		err: err,
	}
	db.mu.Unlock()
}

func (db *DB) getUser(id uint64, lock *once) {
	if atomic.LoadUint32(&lock.done) == 0 {
		lock.m.Lock()
		defer lock.m.Unlock()
		// Повторно проверяем значение для конкурентно выполняющихся рутин, которые захватят лок следующими.
		if lock.done == 0 {
			defer atomic.StoreUint32(&lock.done, 1)
			db.fetchCacheUser(id)
		}
	}
}

func (db *DB) get(id uint64) (*User, error) {
	val, _ := db.getLock.LoadOrStore(id, &once{})
	lock := val.(*once)
	db.getUser(id, lock)

	// Пытаемся получить значение пользователя из кэша.
	db.mu.RLock()
	metaUser, ok := db.users[id]
	db.mu.RUnlock()

	// Если не находим пользователя, выходим и возвращаем ошибку.
	if !ok {
		return nil, errNetworkFailed
	}
	// Проверяем на ошибку в кэше пользователя. Возвращаем при наличии.
	if metaUser.err != nil {
		return nil, metaUser.err
	}
	return metaUser.proto, nil
}

/*
 * TODO: реализовать thread-safe метод
 */
func (db *DB) putUser(user *User, lock *once) {
	if atomic.LoadUint32(&lock.done) == 0 {
		lock.m.Lock()
		defer lock.m.Unlock()
		// Повторно проверяем значение для конкурентно выполняющихся рутин, которые захватят лок следующими.
		if lock.done == 0 {
			defer atomic.StoreUint32(&lock.done, 1)
			//fmt.Printf("        user#%d putting\n", user.id)
			_ = db.store(user)
		}
	}
}

func (db *DB) put(user *User) {
	val, _ := db.putLock.LoadOrStore(user.id, &once{})
	lock := val.(*once)
	db.putUser(user, lock)
}

/*
 * fetch - возвращает объект по ID из базы, симулирует
 */
func (db *DB) fetch(id uint64) (*User, error) {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNotFound
	}

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNetworkFailed
	}

	return &User{
		id: id,
		name: fmt.Sprintf("user#%d", id),
		uniq: rand.Uint64(),
	}, nil
}

/* симулирует сохранение объекта через сеть - т.е. делает это с задержкой и иногда возвращает ошибку */
func (db *DB) store(user *User) error {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return errNetworkFailed
	}

	// all ok
	return nil
}

func NewDB(usersNumber int) *DB {
	db := &DB{
		users: make(map[uint64]*MetaUser),
		usersNumber: usersNumber,
	}
	return db
}

func (db *DB) SyncMap() {
	rand.Seed(time.Now().UTC().UnixNano())

	wg := sync.WaitGroup{}
	for i := 0; i < 50; i ++ {
		wg.Add(1)
		go func() {
			uid := uint64(rand.Int63n(int64(db.usersNumber)))
			user, err := db.get(uid)
			if err != nil {
				fmt.Printf("user#%d: %#v\n", uid, err)
			} else {
				fmt.Printf("user#%d: uniq %d\n", user.id, user.uniq)
				time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
				db.put(user)
			}
			wg.Done()
		}()
	}
	wg.Wait()

	// Очищаем память.
	db.getLock.Range(func(k, v interface{}) bool {
		db.getLock.Delete(k)
		return true
	})
	db.putLock.Range(func(k, v interface{}) bool {
		db.getLock.Delete(k)
		return true
	})
}
