/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 * 50 рутин параллельно вызывают db.get(1) - к базе должен быть только один запрос
 * все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 * 50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть только один
 */

package blockingchan

import (
	"fmt"
	"testing"
)

func TestBlockingChan(t *testing.T) {
	db := NewDB(50, 50000)
	//db := NewDB(2, 50)
	db.BlockingChan()

	err := db.GetHitCounter.CheckHits()
	if err != nil {
		err = fmt.Errorf("get DB hit counter error - %v", err)
		t.Error(err)
	}

	err = db.LockCounter.CheckHits()
	if err != nil {
		err = fmt.Errorf("get lock counter error - %v", err)
		t.Error(err)
	}

	err = db.PutHitCounter.CheckHits()
	if err != nil {
		err = fmt.Errorf("put DB hit counter error - %v", err)
		t.Error(err)
	}
}

func BenchmarkBlockingChan(b *testing.B) {
	db := NewDB(50, 5000000)
	for i := 0; i < b.N; i++ {
		db.BlockingChan()
	}
}
