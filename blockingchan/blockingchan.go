/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 * 50 рутин параллельно вызывают db.get(1) - к базе должен быть только один запрос
 * все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 * 50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть только один
 */

package blockingchan

import (
	"bitbucket.org/sergey-zhuravlev/in-memory-db/counter"
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var errNotFound = errors.New("db: not found")
var errNetworkFailed = errors.New("db: network failed")
var errNetworkMaxRetries = errors.New("db: maximum retries exceeded")

type User struct {
	ID uint64
	name string
	Uniq uint64
}

type DB struct {
	mu sync.RWMutex
	users map[uint64]*User
	maxRetries int  // Максимальное повторное кол-во попыток при ошибке errNetworkFailed
	putCounter counter.Counter  // Счётчик обновлялся ли уже пользователь или нет
	GetBlock []chan struct{}
	PutBlock []chan struct{}
	GetHitCounter counter.Counter
	PutHitCounter counter.Counter
	LockCounter counter.Counter
	UsersNumber uint64
	BlockChanSize uint64
}

/*
 *
 * TODO: реализовать thead-safe метод получения объекта из базы данных и не забыть про корректную обработку ошибок:
 * - notFound - сохранять как ответ в кеше
 * - networkError - не сохранять как ответ и идти в сеть снова
 */
func (db *DB) fetchUser(id uint64) (*User, error) {
	//fmt.Printf("        user#%d fetching\n", id)
	var user *User
	var err error
	// Выполняем повторное обращение к БД в случае ошибки errNetworkFailed, кол-во повторов ограничено db.maxRetries
	for retries := 0; retries < db.maxRetries; retries ++ {
		db.GetHitCounter.Count(id)  // считаем обращения к БД для отдельного пользователя
		user, err = db.fetch(id)
		if err != errNetworkFailed {
			break
		}
		db.GetHitCounter.Decount(id)  // ошибка не считается за хит БД
		if retries == db.maxRetries -1 {
			err = errNetworkMaxRetries
		}
	}

	if err != nil {
		db.GetHitCounter.Decount(id)  // ошибка не считается за хит БД
		return nil, err
	}

	return user, nil
}

func (db *DB) Get(id uint64) (*User, error) {
	var err error
	var user *User
	db.GetBlock[id] <- struct {}{}  // блокируем асинхронное выполнение для отдельно взятого пользователя
	db.mu.RLock()
	user, ok := db.users[id]
	db.mu.RUnlock()
	if !ok {
		user, err = db.fetchUser(id)
		if err == nil {
			db.mu.Lock()
			db.LockCounter.Count(id)  // считаем кол-во блокировок для отдельного пользователя
			db.users[id] = user
			db.mu.Unlock()
		}
	}
	<-db.GetBlock[id]  // разблокируем асинхронное выполнение

	if err != nil {
		return nil, err
	}
	return user, nil
}

/*
 * TODO: реализовать thread-safe метод
 */
func (db *DB) Put(user *User) {
	db.PutBlock[user.ID] <- struct {}{}  // блокируем асинхронное выполнение для отдельно взятого пользователя
	userHits := db.putCounter.Count(user.ID)  // считаем кол-во обращений к БД на запись для отдельного пользователя
	if userHits == 1 {  // если счётчик для данного пользователя только появился, обновляем пользователя
		db.PutHitCounter.Count(user.ID)  // считаем кол-во обращений к БД на запись для отдельного пользователя
		_ = db.store(user)
	}
	<-db.PutBlock[user.ID]  // разблокируем асинхронное выполнение
}

/*
 * fetch - возвращает объект по ID из базы, симулирует
 */
func (db *DB) fetch(id uint64) (*User, error) {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNotFound
	}

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNetworkFailed
	}

	return &User{
		ID: id,
		name: fmt.Sprintf("user#%d", id),
		Uniq: rand.Uint64(),
	}, nil
}

/* симулирует сохранение объекта через сеть - т.е. делает это с задержкой и иногда возвращает ошибку */
func (db *DB) store(user *User) error {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return errNetworkFailed
	}

	// all ok
	return nil
}

func NewDB(usersNumber, blockChanSize uint64) *DB {
	db := &DB{
		UsersNumber: usersNumber,
		users: make(map[uint64]*User),
		maxRetries: 5,
		putCounter: counter.Counter{ Hits: make(map[uint64]int) },
		GetBlock: make([]chan struct{}, blockChanSize),
		PutBlock: make([]chan struct{}, blockChanSize),
		GetHitCounter: counter.Counter{ Hits: make(map[uint64]int) },
		PutHitCounter: counter.Counter{ Hits: make(map[uint64]int) },
		LockCounter: counter.Counter{ Hits: make(map[uint64]int) },
	}
	for i := uint64(0); i < blockChanSize; i++ {
		db.GetBlock[i] = make(chan struct{}, 1)
		db.PutBlock[i] = make(chan struct{}, 1)
	}
	return db
}

func (db *DB) BlockingChan() {
	rand.Seed(time.Now().UTC().UnixNano())

	wg := sync.WaitGroup{}
	for i := 0; i < 50; i ++ {
		wg.Add(1)
		go func() {
			uid := uint64(rand.Int63n(int64(db.UsersNumber)))
			user, err := db.Get(uid)
			if err != nil {
				fmt.Printf("user#%d: %#v\n", uid, err)
			} else {
				fmt.Printf("user#%d: uniq %d\n", user.ID, user.Uniq)
				time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
				db.Put(user)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}
