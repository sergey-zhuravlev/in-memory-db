/*
 * задача: реализовать методы get/put чтобы они могли работать с одним объектом db из множества горутин
 * данные методы должны получать/сохранять используя fetch/store
 *
 * т.е. представь ситуацию:
 * 50 рутин параллельно вызывают db.get(1) - к базе должен быть только один запрос
 * все последующие запросы должны тут же возвращать ответ из кеша db.users
 *
 * 50 горутин поменяли user и сохраняют его через db.store - апдейт должен быть только один
 */

package synconce

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var errNotFound = errors.New("db: not found")
var errNetworkFailed = errors.New("db: network failed")
var errAtomicTest = errors.New("atomic test")

type User struct {
	id uint64
	name string
	uniq uint64
}

type MetaUser struct {
	proto *User
	err error
}

type DB struct {
	users map[uint64]*MetaUser
	mu sync.RWMutex
	onceGet sync.Once
	oncePut sync.Once
	err error
}

/*
 *
 * TODO: реализовать thead-safe метод получения объекта из базы данных и не забыть про корректную обработку ошибок:
 * - notFound - сохранять как ответ в кеше
 * - networkError - не сохранять как ответ и идти в сеть снова
 */
func (db *DB) fetchUser() {
	id := uint64(0)
	fmt.Printf("        user#%d fetching\n", id)
	proto, err := db.fetch(id)

	if err != nil {
		if err == errNotFound {
			// Ошибка поиска пользователя - создаём нового, ниже сохраняем ошибку в кэш.
			proto = &User{ id: id }
		} else {
			// Ошибка сети - записываем ошибку и выходим из функции.
			db.err = err
			return
		}
	}
	// Записываем результат в кэш, включая значение ошибки.
	db.mu.Lock()
	db.users[id] = &MetaUser{
		proto: proto,
		err: err,
	}
	db.mu.Unlock()
}

func (db *DB) get(id uint64) (*User, error) {
	// Функция вызывается один раз для всех рутин. Первый вызов обрабатывается, остальные ожидают окончания обработки.
	db.onceGet.Do(db.fetchUser)
	// Пытаемся получить значение пользователя из кэша.
	db.mu.RLock()
	metaUser, ok := db.users[id]
	db.mu.RUnlock()
	// Если не находим пользователя, выходим и возвращаем записанную ошибку (но не из кэша пользователя).
	if !ok {
		return nil, db.err
	}
	// Проверяем на ошибку в кэше пользователя. Возвращаем при наличии.
	if metaUser.err != nil {
		return nil, metaUser.err
	}

	return metaUser.proto, nil
}

/*
 * TODO: реализовать thread-safe метод
 */
func (db *DB) putUser()  {
	db.mu.Lock()
	user := db.users[0].proto
	db.mu.Unlock()
	_ = db.store(user)
}

func (db *DB) put(user *User) {
	db.oncePut.Do(db.putUser)
}

/*
 * fetch - возвращает объект по ID из базы, симулирует
 */
func (db *DB) fetch(id uint64) (*User, error) {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNotFound
	}

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return nil, errNetworkFailed
	}

	return &User{
		id: id,
		name: fmt.Sprintf("user#%d", id),
		uniq: rand.Uint64(),
	}, nil
}

/* симулирует сохранение объекта через сеть - т.е. делает это с задержкой и иногда возвращает ошибку */
func (db *DB) store(user *User) error {
	/* emulate slow database fetch request */
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

	/* каждые 10 запросов - ломаемся и не находим результат */
	if rand.Intn(10) == 0 {
		return errNetworkFailed
	}

	// all ok
	return nil
}

func NewDB() *DB {
	db := &DB{
		users: make(map[uint64]*MetaUser),
	}
	return db
}

func (db *DB) SyncOnce() {
	rand.Seed(time.Now().UTC().UnixNano())

	wg := sync.WaitGroup{}
	for i := 0; i < 50; i ++ {
		wg.Add(1)
		go func() {
			//uid := uint64(rand.Int63n(int64(50)))
			uid := uint64(0)
			user, err := db.get(uid)
			if err != nil {
				if err != errAtomicTest {
					fmt.Printf("user#%d: %#v\n", uid, err)
				}
			} else {
				fmt.Printf("user#%d: uniq %d\n", user.id, user.uniq)
				time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
				db.put(user)
			}
			wg.Done()
		}()
	}
	wg.Wait()
	// Очищаем память, что можем.
	delete(db.users, 0)
	db = nil
}
