module bitbucket.org/sergey-zhuravlev/in-memory-db

go 1.17

replace bitbucket.org/sergey-zhuravlev/in-memory-db/blockingchan => ./blockingchan

replace bitbucket.org/sergey-zhuravlev/in-memory-db/counter => ./counter

replace bitbucket.org/sergey-zhuravlev/in-memory-db/simplemod => ./simplemod
